package com.bailram.usermanagementservice.service;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import com.bailram.usermanagementservice.domain.entity.User;
import com.bailram.usermanagementservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserManagementService {

    @Autowired
    private UserRepository userRepository;

    public UserResponse findAllOrByRole (UserRequest req) {
        List<User> userList = null;
        if(req == null) {
            userList = userRepository.findAll();
        } else {
            userList = userRepository.findByRole(req.getRole());
        }

        List<UserDto> dtoList = new ArrayList<>();
        if (userList != null) {
            for (User row :
                    userList) {
                UserResponse response = new UserResponse();
                UserDto userMap = UserDto.builder()
                        .userId(String.valueOf(row.getUserId()))
                        .fullName(row.getFullName())
                        .address(row.getAddress())
                        .role(row.getRole())
                        .build();
                dtoList.add(userMap);
            }
        }
        return UserResponse.builder().userList(dtoList).build();
    }

    public String add(UserDto data) {
        if (data == null ) {
            return "New user data not found. Do insert new user data!";
        }
        User user = User.builder()
                .fullName(data.getFullName())
                .address(data.getAddress())
                .role(data.getRole())
                .build();
        userRepository.save(user);
        return "Successfully create new user.";
    }

    public String update(UserDto data) {
        if (data.getUserId() == "") {
            return "Insert the id user!";
        }
        Optional<User> result = userRepository.findById(Long.valueOf(data.getUserId()));
        if (!result.isPresent() || result.isEmpty()) {
            return "User with id "+data.getUserId()+" not found!";
        }
        if (!data.getFullName().isEmpty()) {
            result.get().setFullName(data.getFullName());
        }
        if (!data.getAddress().isEmpty()) {
            result.get().setAddress(data.getAddress());
        }
        if (!data.getRole().isEmpty()) {
            result.get().setRole(data.getRole());
        }
        userRepository.save(result.get());
        return "Successfully update user with id "+data.getUserId()+".";
    }

    public String delete(long id) {
        Optional<User> result = userRepository.findById(id);
        if (!result.isPresent() || result.isEmpty()) {
            return "User with id "+id+" not found!";
        }
        userRepository.delete(result.get());
        return "Successfully delete user with id "+id+".";
    }
}
