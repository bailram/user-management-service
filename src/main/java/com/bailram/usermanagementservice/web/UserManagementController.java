package com.bailram.usermanagementservice.web;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import com.bailram.usermanagementservice.service.UserManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@RequestMapping("/api/usermanagement/v1")
public class UserManagementController {
    @Autowired
    private UserManagementService userManagementService;

    @PostMapping("/getAllUser")
    @ResponseBody
    public UserResponse getAllUser() {
        try {
            UserResponse result = userManagementService.findAllOrByRole(null);
            log.info("Return all user list");
            return result;
        } catch (Exception e) {
            log.error("Exception happened when get all user list message [{}]", e);
            throw e;
        }
    }

    @PostMapping("/getUserByRole")
    @ResponseBody
    public UserResponse getUserByRole(@RequestBody UserRequest request) {
        try {
            UserResponse result = userManagementService.findAllOrByRole(request);
            log.info("Return all user list by role");
            return result;
        } catch (Exception e) {
            log.error("Exception happened when get all user list by role message [{}]", e);
            throw e;
        }
    }

    @PostMapping("/add")
    @ResponseBody
    public String addUser(@RequestBody UserDto request) {
        try {
            String result = userManagementService.add(request);
            log.info("Return result add new user");
            return result;
        } catch (Exception e) {
            log.error("Exception happened when add new user message [{}]", e);
            throw e;
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public String updateUser(@RequestBody UserDto request) {
        try {
            String result = userManagementService.update(request);
            log.info("Return result update user");
            return result;
        } catch (Exception e) {
            log.error("Exception happened when update user message [{}]", e);
            throw e;
        }
    }

    @PostMapping("/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable long id) {
        try {
            String result = userManagementService.delete(id);
            log.info("Return result delete user");
            return result;
        } catch (Exception e) {
            log.error("Exception happened when delete user message [{}]", e);
            throw e;
        }
    }
}
